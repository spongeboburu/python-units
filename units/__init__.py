# -*- coding: utf-8 -*-
#
# Main module for unit features and home of the default context.
#
# Part Python Units, a small library for parsing and converting various units.
#
# MIT License

# Copyright (c) 2018 Peter Gebauer

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import decimal
import itertools
import types
import re
import locale


NUMBER_FORMAT_FLAGS_NONE = 0
"""
Used in to_decimal() and format_decimal().
"""


NUMBER_FORMAT_FLAG_SCIENTIFIC = 1
"""
Allow scientific notiation.
Used in to_decimal() and format_decimal().
"""


NUMBER_FORMAT_FLAG_DECIMAL = 2
"""
Allow decimal notation.
Used in to_decimal() and format_decimal().
"""


NUMBER_FORMAT_FLAG_LOCALIZED = 4
"""
Allow localized notation.
Used in to_decimal() and format_decimal().
"""



def to_decimal(s, default=None, format_flags=None, thousands_sep="", decimal_point="."):
    """
    Parses a string with decimal notation.
    decimal_format argument:
    format_flags argument:
    See NUMBER_FORMAT_FLAG_*
    """
    if s == "" or s is None:
        return decimal.Decimal(0)
    if isinstance(s, (int, float, decimal.Decimal)):
        return decimal.Decimal(s)
    if format_flags is None:
        format_flags = 0xff
    if isinstance(s, str):
        if not decimal_point:
            raise ValueError("decimal_point has no value")
        v = s.strip()
        scientific_pattern = "([Ee][\+\-]?\d+)%s"%("?" if (format_flags & NUMBER_FORMAT_FLAG_DECIMAL) else "") if (format_flags & NUMBER_FORMAT_FLAG_SCIENTIFIC) else ""
        decimal_pattern = "(?P<decimal>[\+\-]?((\d+\.\d*)|(\.\d+)|\d+)%s)"%scientific_pattern
        if thousands_sep and (format_flags & NUMBER_FORMAT_FLAG_LOCALIZED):
            locale_pattern = "(?P<locale>[\+\-]?(\d{1,3}(%s\d{3})*(%s\d*)?)%s)"%(re.escape(thousands_sep), re.escape(decimal_point), scientific_pattern)
        else:
            locale_pattern = ""
        if locale_pattern and decimal_pattern:
            decimal_locale_pattern = "(%s|%s)"%(decimal_pattern, locale_pattern)
        elif decimal_pattern:
            decimal_locale_pattern = "%s"%decimal_pattern
        elif locale_pattern:
            decimal_locale_pattern = "%s"%locale_pattern
        else:
            return None
        number_pattern = "%s"%decimal_locale_pattern
        m = re.search("^%s$"%number_pattern, v)
        if m:
            d = m.groupdict()
            v = d.get("decimal", "")
            if not v:
                v = d.get("locale", "")
                v = v.replace(thousands_sep, "")
            try:
                return decimal.Decimal(v)
            except (decimal.InvalidOperation, ValueError, TypeError):
                pass
        return None
    try:
        return decimal.Decimal(s)
    except (decimal.InvalidOperation, ValueError, TypeError):
        pass
    return default


def format_decimal(d, format_flags=None, thousands_sep="", decimal_point=".", max_decimals=-1):
    """
    Format a decimal to a string.
    format_flags argument:
    See NUMBER_FORMAT_FLAG_*
    
    NOTES: 
    - If both NUMBER_FORMAT_FLAG_SCIENTIFIC and NUMBER_FORMAT_FLAG_DECIMAL are present there will be an auto detection.
    - If neither NUMBER_FORMAT_FLAG_SCIENTIFIC or NUMBER_FORMAT_FLAG_DECIMAL are present then NUMBER_FORMAT_FLAG_DECIMAL is implicit.
    """
    if format_flags is None:
        format_flags = 0xff
    ret = []
    t = d.as_tuple()
    if t.sign:
        ret.append("-")
    use_scientific = False
    if (format_flags & NUMBER_FORMAT_FLAG_SCIENTIFIC) and (format_flags & NUMBER_FORMAT_FLAG_DECIMAL):
        if t.exponent >= 0:
            zeroes = 0
            for i in t.digits:
                if i == 0:
                    zeroes += 1
                    if zeroes > 6:
                        use_scientific = True
                        break
                else:
                    zeroes = 0
    elif (format_flags & NUMBER_FORMAT_FLAG_SCIENTIFIC):
        use_scientific = True
    else:
        use_scientific = False
    if use_scientific:
        ret.append(t.digits[0])
        ret.append(decimal_point)
        remaining = []
        for i in range(1, len(t.digits) - 1):
            if t.digits[-i] != 0:
                remaining = t.digits[1:-i+1]
                break
        if not remaining:
            remaining.append(0)
        ret += remaining
        exponent = t.exponent + len(t.digits) - 1
        if exponent != 0:
            ret.append("E%s"%exponent)
    else:
        if t.exponent < 0:
            decimal_index = len(t.digits) + t.exponent
            if decimal_index < 0:
                ret.append(0)
                ret.append(decimal_point)
                for i in range(abs(decimal_index)):
                    ret.append(0)
                ret += t.digits
            elif decimal_index > 0:
                ret += t.digits[:decimal_index]
                ret.append(decimal_point)
                ret += t.digits[decimal_index:]
            else:
                ret.append(0)
                ret.append(decimal_point)
                ret += t.digits
            for i in range(1, len(ret)):
                if isinstance(ret[-i], int):
                    if ret[-i] != 0:
                        if i > 1:
                            ret = ret[:-i+1]
                        break
                else:
                    break
        else:
            ret += t.digits
            for i in range(t.exponent):
                ret.append(0)
    rets = "".join(("%s"%x for x in ret))
    if (format_flags & NUMBER_FORMAT_FLAG_LOCALIZED) and thousands_sep:
        ret = []
        splitty = rets.split(decimal_point)
        spl = "".join(reversed(splitty[0]))
        while spl:
            ret.append("".join(reversed(spl[:3])))
            spl = spl[3:]
            if len(ret[-1]) == 3 and spl:
                ret.append(thousands_sep)
        rets = "".join(reversed(ret))
        if len(splitty) > 1:
            rets += ".%s"%"".join(splitty[1:])
    return rets


def is_decimal(s, format_flags=None, thousands_sep="", decimal_point="."):
    """
    Check it the string is a decimal.
    """
    if to_decimal(s, None, format_flags, thousands_sep, decimal_point) is None:
        return False
    return True
    

def to_integer(s, default=None):
    """
    Parses a string with integer notation.
    """
    try:
        return int(s)
    except (ValueError, TypeError):
        pass
    return default


def is_integer(s):
    """
    Check if the string is a valid integer.
    """
    return not to_integer(s, None) is None


def to_unit_type(v):
    """
    Convert to a unit type if Unit instance or return v as it is if already a unit type.
    None is returned if v is neither unit instance or unit type.
    """
    if isinstance(v, Unit):
        return type(v)
    if isinstance(v, type) and issubclass(v, Unit):
        return v
    return None


def to_unit_instance(v):
    """
    Convert to a unit instance if unit type or return a clone of v if Unit instance.
    None is returned if v is neither unit instance or unit type.
    """
    if isinstance(v, Unit):
        return type(v)(v)
    if isinstance(v, type) and issubclass(v, Unit):
        return v()
    raise None


def type_name(v):
    """
    Return the type name of an instance or type.
    """
    if isinstance(v, type):
        return v.__name__
    return type(v).__name__


class UnitMultiplier:
    """
    Used for unit multiplying (such as kilo or mega).
    """

    def __init__(self, symbols, names = None, value = None):
        if isinstance(symbols, UnitMultiplier):
            names = symbols.names
            value = symbols.value
            symbols = symbols.symbols
        if isinstance(symbols, str):
            self.symbols = symbols.split("|")
        elif isinstance(symbols, (list, tuple)):
            self.symbols = list(symbols)
        elif symbols is None:
            self.symbols = []
        else:
            raise TypeError("invalid type for symbols: %s"%type(symbols).__name__)
        if isinstance(names, str):
            self.names = names.split("|")
        elif isinstance(names, (list, tuple)):
            self.names = list(names)
        elif names is None:
            self.names = []
        else:
            raise TypeError("invalid type for names: %s"%type(names).__name__)
        self.value = to_decimal(value)
        if self.value is None:
            raise ValueError("invalid multiplier value: %s"%type(value).__name__)


    def __str__(self):
        return "<%s '%s' '%s' %s>"%(type(self).__name__, "|".join(self.symbols), "|".join(self.names), self.value)
   

    def __eq__(self, other):
        if not isinstance(other, UnitMultiplier):
            raise TypeError("unsupported operand type(s) for ==: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        return bool("".join(self.symbols) == "".join(other.symbols) and "".join(self.names) == "".join(other.names) and self.value == other.value)

    
class Unit:
    """
    Represents any unit, integers and decimals (and strings with integer or decimal notation).
    """

    SYSTEM = ""
    """
    Just a name for the system, preferred lowercase and underscores.
    """

    MEASURE = ""
    """
    What the unit is measuring (i.e "length", "mass", etc).
    """

    PREFIXES = tuple()
    """
    A tuple with all valid prefix symbols.
    """

    SUFFIXES = tuple()
    """
    A tuple with all valid prefix symbols.
    """
    
    NAMES = tuple()
    """
    A tuple with all valid names (names may only appear after the merical value).
    NOTE: every even element is singular and every odd element is plural.
    """

    CASE_SENSETIVE_SYMBOLS = True
    """
    True = case matters for symbols.
    """

    MULTIPLIERS = tuple()
    """
    Used to for multipliers such as kilo or milli.
    Multipliers always come AFTER the number.
    """

    MAX_DECIMALS = None
    """
    An integer describing the maximum number of digits after the decimal mark, negative value to disable limit.
    None means we rely on the context to tell us.
    """
    
    NUMBER_FORMAT_FLAGS = None
    """
    See NUMBER_FORMAT_FLAG_*
    None means we rely on the context to tell us.
    """
    
    
    @classmethod
    def find_multiplier(cls, s, context=None):
        if not context:
            context = get_context()
        if isinstance(s, UnitMultiplier):
            for i, sp in enumerate(cls.MULTIPLIERS):
                if s == sp:
                    return i
            return -1
        v = to_decimal(s)
        if v is None and isinstance(s, str):
            sl = s.lower()
            for i, sp in enumerate(cls.MULTIPLIERS):
                if s in sp.symbols or sl in sp.names:
                    return i
                else:
                    for n in sp.names:
                        if s in context.find_translations(n):
                            return i
            return -1
        if v is None:
            raise TypeError("invalid symbol prefix type: %s"%type(s).__name__)
        for i, sp in enumerate(cls.MULTIPLIERS):
            if v == sp.value:
                return i
        return -1


    def __init__(self, value=None, context=None):
        """
        Initialize the unit. The value can be integer, float, decimal, a unit instance or a string representing any of aforementioned types.
        """
        self._context = context
        self._value = decimal.Decimal(0)
        self._match = None
        self._multiplier = None
        self._number_format_flags = None
        self._thousands_sep = None
        self._decimal_point = None
        self._max_decimals = None
        if not self._context:
            if isinstance(value, Unit) and value.context:
                self._context = value.context
            else:
                self._context = get_context()
        if type(value) == type(self):
            self.multiplier = value.multiplier
        v = 0 if value is None else value
        if isinstance(v, str):
            item = self.context.get_item_for_unit(self)
            if not item:
                raise AssertionError("could not find an item for '%s' in context %s"%(type(self).__name__, self.context))
            v = v.replace(" ", "")
            m = item.match(v)
            if m:
                v = self.handle_match(m)
                if not isinstance(v, decimal.Decimal):
                    raise AssertionError("%s.handle_match() returned non-decimal value from %s (bug?)"%(type(self).__name__, value))
            else:
                v = self.to_decimal(v)
        v = self.convert_value_from_instance(v)
        if v is None:
            raise ValueError("invalid value for %s: %s"%(type(self).__name__, value))
        self.set_value(v)

        
    def __str__(self):
        vstr = format_decimal(self.value)
        if self.SUFFIXES:
            return "%s%s%s"%(vstr, self.multiplier_symbol, self.SUFFIXES[0])
        if self.PREFIXES:
            return "%s%s%s"%(self.PREFIXES[0], vstr, self.multiplier_symbol)
        if self.NAMES:
            return "%s%s %s"%(vstr, self.multiplier_symbol, self.NAMES[0])
        return vstr


    def __repr__(self):
        if self.NAMES:
            name = self.NAMES[0]
        elif self.SUFFIXES:
            name = self.SUFFIXES[0]
        elif self.PREFIXES:
            name = self.PREFIXES[0]
        else:
            name = ""
        return "<%s%s%s %s>"%(
            type(self).__name__,
            " '%s'"%name if name else "",
            " %s"%self._multiplier if self._multiplier else "",
            self.value
        )
        return vstr
    

    def __int__(self):
        return int(self.value)


    def __float__(self):
        return float(self.value)


    def __round__(self):
        return self.round()
        

    def __eq__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for ==: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        return self.value == v


    def __lt__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for <: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        return self.value < v


    def __gt__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for >: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        return self.value > v


    def __le__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for <=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        return self.value <= v


    def __ge__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for >=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        return self.value >= v


    def __abs__(self):
        r = type(self)(self)
        if r.value < 0:
            r.value *= -1
        return r
    

    def __add__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for +: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        r = type(self)(self)
        r.value += v
        return r


    def __iadd__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for +=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        self.value += v
        return self


    def __sub__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for -: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        r = type(self)(self)
        r.value -= v
        return r


    def __isub__(self, other):
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for -=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        self.value -= v
        return self


    def __mul__(self, other):
        if (isinstance(other, Unit) and not type(other) is Unit) or (isinstance(other, str) and self.to_decimal(other) is None):
            raise TypeError("unsupported operand type(s) for *: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for *: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        r = type(self)(self)
        r.value *= v
        return r


    def __imul__(self, other):
        if (isinstance(other, Unit) and not type(other) is Unit) or (isinstance(other, str) and self.to_decimal(other) is None):
            raise TypeError("unsupported operand type(s) for *=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for *=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        self.value *= v
        return self


    def __truediv__(self, other):
        if (isinstance(other, Unit) and not type(other) is Unit) or (isinstance(other, str) and self.to_decimal(other) is None):
            raise TypeError("unsupported operand type(s) for /: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for /: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        r = type(self)(self)
        r.value /= v
        return r


    def __itruediv__(self, other):
        if (isinstance(other, Unit) and not type(other) is Unit) or (isinstance(other, str) and self.to_decimal(other) is None):
            raise TypeError("unsupported operand type(s) for /=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        v = self.convert_value_from_instance(other)
        if v is None:
            raise TypeError("unsupported operand type(s) for /=: '%s' and '%s'"%(type(self).__name__, type(other).__name__))
        self.value /= v
        return self


    def match(self, s, exact=True):
        """
        Match the string using this unit and it's context.
        """
        if exact:
            v = s.strip()
            m = match_regexp(v, self)
            if m and m.matched == v:
                return m
        else:
            m = match_regexp(v, self)
            if m:
                return m
        return None
        
            
    @property
    def context(self):
        if not self._context:
            self._context = get_context()
        return self._context


    def convert(self, value, max_decimals=None):
        """
        Convert self to whatever the value might be.
        """
        if max_decimals is None:
            max_decimals = self._max_decimals
        if isinstance(value, (int, float, decimal.Decimal)) or (isinstance(value, type) and issubclass(value, (int, float, decimal.Decimal))):
            return Unit(self, max_decimals=max_decimals)
        elif isinstance(value, str):
            v = self.to_decimal(value)
            if v is None:
                u = self.context.unit(value)
                u.max_decimals = max_decimals
                u.value = u.convert_value_from_instance(self)
                return u
            return v
        elif isinstance(value, type) and issubclass(value, Unit):
            return value(self, max_decimals=self._max_decimals)
        elif isinstance(value, Unit):
            u = type(value)(value, max_decimals=max_decimals)
            u.value = u.convert_value_from_instance(self)
            return u
        else:
            raise TypeError("unsupported type: %s"%(value.__name__ if isinstance(value, type) else type(value).__name__))


    def coerce(self, value, factor, max_decimals=None):
        """
        Coerce self to whatever the value might be by using a multiplying factor.
        NOTE: unit multipliers are in effect!
        """
        if max_decimals is None:
            max_decimals = self._max_decimals
        f = to_decimal(factor)
        if f is None:
            raise TypeError("expected factor argument of type int, float, Decimal or str, not %s"%type(factor).__name__)
        if f <= 0:
            raise TypeError("factor must be > 0: %s"%factor)
        if isinstance(value, (int, float, decimal.Decimal)):
            u = Unit(self.value * f)
            u.max_decimals = max_decimals
            return u
        elif isinstance(value, Unit):
            u = type(value)((self.value * self.multiplier_value * f) / value.multiplier_value)
            u.max_decimals = max_decimals
            return u
        elif isinstance(value, str):
            v = self.to_decimal(value)
            if v is None:
                u = self.context.unit(value)
                u.max_decimals = max_decimals
                u.value = (self.value * self.multiplier_value * f) / u.multiplier_value
                return u
            else:
                return Unit(v)
        raise ValueError("could not coerce to value: %s"%value)


    def to_decimal(self, s):
        """
        Convert a string to a decimal based on the unit's settings.
        """
        return to_decimal(s, None, self.number_format_flags, self.thousands_sep, self.decimal_point)
        
    
    def get_name(self, n=None):
        """
        Get the standard name of the unit based on it's value (singular or plural).
        """
        if n is None:
            n = self.value
        if not self.NAMES:
            return ""
        if n != 1 and len(self.NAMES) > 1:
            return self.NAMES[1]
        return self.NAMES[0]


    @property
    def name(self):
        """
        The standard name of the unit based on it's value (singular or plural).
        """
        return self.get_name()


    def get_translated_name(self, n=None, language_code=None):
        """
        Get the translated name of the unit based on it's value (singular or plural).
        """
        return self.context.translate(self.get_name(n), language_code)


    @property
    def translated_name(self):
        """
        The translated name of the unit based on it's value (singular or plural).
        """
        return self.get_translated_name()
    

    def get_value(self):
        """
        Get the value of the unit.
        """
        return to_decimal(self._value)


    def set_value(self, value):
        """
        Set the value of the unit.
        """
        v = self.to_decimal(value)
        if v is None:
            raise ValueError("invalid value: %s"%value)
        v = v.normalize()
        t = v.as_tuple()
        max_decimals = self.get_max_decimals()
        if t.exponent < 0 and len(t.digits) > abs(t.exponent):
            num_digits = 0
            digits = t.digits[t.exponent:][::-1]
            for i, dt in enumerate(digits):
                if dt != 0:
                    num_digits = abs(t.exponent) - i
                    break
            if max_decimals >= 0 and num_digits > max_decimals:
                num_digits = max_decimals
        else:
            num_digits = max_decimals
        if num_digits > 0:
            v = v.quantize(decimal.Decimal("1." + "0" * num_digits))
        elif num_digits == 0:
            v = v.quantize(decimal.Decimal("1"))
        self._value = v
        

    @property
    def value(self):
        """
        The value of the unit.
        """
        return self.get_value()


    @value.setter
    def value(self, value):
        self.set_value(value)


    def get_multiplier(self):
        """
        Get the symbol prefix of the unit.
        """
        return self._multiplier


    def set_multiplier(self, multiplier):
        """
        Set the symbol prefix of the unit.
        """
        if not multiplier or (isinstance(multiplier, str) and not multiplier.strip()):
            self._multiplier = None
            return
        index = self.find_multiplier(multiplier, self.context)
        if index < 0:
            raise ValueError("invalid multiplier: %s"%multiplier)
        self._multiplier = UnitMultiplier(self.MULTIPLIERS[index])


    @property
    def multiplier(self):
        """
        The symbol prefix of the unit.
        """
        return self.get_multiplier()


    @multiplier.setter
    def multiplier(self, multiplier):
        self.set_multiplier(multiplier)


    def get_multiplier_name(self, multiplier=None):
        """
        Return the multiplier name or an empty string for no multiplier.
        """
        if multiplier:
            use_multiplier = self.find_multiplier(multiplier)
            if not use_multiplier:
                raise ValueError("invalid multiplier: %s"%multiplier)
        else:
            use_multiplier = self.multiplier
        if use_multiplier:
            if use_multiplier.names:
                return use_multiplier.names[0]
            if use_multiplier.symbols:
                return use_multiplier.symbols[0]
        return ""


    @property
    def multiplier_name(self):
        """
        The name of the multiplier or an empty string for no multiplier.
        """
        return self.get_multiplier_name()


    def get_translated_multiplier_name(self, multiplier=None, language_code=None):
        """
        Return the translated multiplier name or an empty string for no multiplier.
        """
        return self.context.translate(self.get_multiplier_name(multiplier), language_code)


    @property
    def translated_multiplier_name(self):
        """
        The translated name of the multiplier or an empty string for no multiplier.
        """
        return self.get_translated_multiplier_name()
    

    @property
    def multiplier_value(self):
        """
        The multiplier value, i.e what to multiply/divide with.
        """
        if self._multiplier and self._multiplier.value:
            return self._multiplier.value
        return 1


    @property
    def multiplier_symbol(self):
        """
        The proper symbol (or name if no symbol is found) of the multiplier.
        """
        if self._multiplier:
            if self._multiplier.symbols:
                return self._multiplier.symbols[0]
            if self._multiplier.names:
                return self._multiplier.names[0]
        return ""


    def get_max_decimals(self):
        """
        Get the max number of decimals for the unit, a negative value means the limit is disabled.
        None: no setting
        -1: no limit
        >= 0: limit to this many decimals.
        If the instance has max decimals set to None we will return the context's max decimals and finally the class.
        If there are no explicit settings for max decimals found the function returns -1 (no limit).
        """
        if not self._max_decimals is None:
            return self._max_decimals
        if not self.context.max_decimals is None:
            return self.context.max_decimals
        if not self.MAX_DECIMALS is None:
            return max(-1, int(self.MAX_DECIMALS))
        return -1


    def set_max_decimals(self, value):
        """
        Set the max number of decimals for the unit, a negative value means the limit is disabled.
        """
        if value is None:
            self._max_decimals = None
        elif isinstance(value, int):
            self._max_decimals = max(-1, value)
        elif isinstance(value, str):
            v = to_integer(value)
            if v is None:
                raise ValueError("invalid max decimals: %s"%value)
            self._max_decimals = max(-1, v)
        else:
            raise TypeError("max decimals requires None, int or str, not %s"%type(value).__name__)


    @property
    def max_decimals(self):
        """
        The maximum number of decimals for the unit, a negative value disables the limit.
        """
        return self.get_max_decimals()


    @max_decimals.setter
    def max_decimals(self, value):
        self.set_max_decimals(value)


    def get_number_format_flags(self):
        if isinstance(self._number_format_flags, int):
            return self._number_format_flags
        if isinstance(self.NUMBER_FORMAT_FLAGS, int):
            return self.NUMBER_FORMAT_FLAGS
        if isinstance(self.context.number_format_flags, int):
            return self.context.number_format_flags
        return None


    def set_number_format_flags(self, flags):
        if flags is None:
            self._number_format_flags = None
        else:
            self._number_format_flags = int(flags)


    @property
    def number_format_flags(self):
        return self.get_number_format_flags()

    
    @number_format_flags.setter
    def number_format_flags(self, value):
        self.set_number_format_flags(value)
        

    def get_thousands_sep(self):
        if isinstance(self._thousands_sep, str):
            return self._thousands_sep
        r = self.context.get_thousands_sep()
        if isinstance(r, str):
            return r
        return None


    def set_thousands_sep(self, flags):
        if flags is None:
            self._thousands_sep = None
        else:
            self._thousands_sep = str(flags)


    @property
    def thousands_sep(self):
        return self.get_thousands_sep()

    
    @thousands_sep.setter
    def thousands_sep(self, value):
        self.set_thousands_sep(value)


    def get_decimal_point(self):
        if isinstance(self._decimal_point, str) and self._decimal_point.strip():
            return self._decimal_point
        r = self.context.get_decimal_point()
        if isinstance(r, str) and r.strip():
            return r
        return "."


    def set_decimal_point(self, flags):
        if flags is None:
            self._decimal_point = None
        else:
            self._decimal_point = str(flags)


    @property
    def decimal_point(self):
        return self.get_decimal_point()

    
    @decimal_point.setter
    def decimal_point(self, value):
        self.set_decimal_point(value)


    def get_localized_str(self, format_number=True, names=True, language_code=None):
        """
        Get a localized str of the unit.
        format_number: If True the number will be formatted using localized thousands separator and decimal point.
        names: If True the translated unit and multiplier names will be used instead of symbols.
        language_code: If you want to use a spefic language, None will use the context settings.
        """
        flags = NUMBER_FORMAT_FLAG_DECIMAL
        if format_number:
            flags |= NUMBER_FORMAT_FLAG_LOCALIZED
        vstr = format_decimal(self.value, flags, self.thousands_sep, self.decimal_point, self.max_decimals)
        space = False
        if names:
            multiplier = self.get_translated_multiplier_name(None, language_code)
            if not multiplier:
                multiplier = self._multiplier.symbols[0] if self._multiplier and self._multiplier.symbols else ""
            else:
                space = True
            suffix = self.get_translated_name(None, language_code)
            if not suffix:
                suffix = self.SYMBOLS[0] if self.SYMBOLS else ""
            else:
                space = True
        else:
            multiplier = self._multiplier.symbols[0] if self._multiplier and self._multiplier.symbols else ""
            suffix = self.SUFFIXES[0] if self.SUFFIXES else ""
        if suffix:
            prefix = ""
        else:
            prefix = self.PREFIXES[0] if self.PREFIXES else ""
        if prefix and vstr[0] in "+-":
            prefix = "%s%s"%(vstr[0], prefix)
            vstr = vstr[1:]
        return "%s%s%s%s%s"%(prefix, vstr, " " if space else "", multiplier, suffix)


    @property
    def lstr(self):
        """
        Quick helper for Unit.get_localized_str()
        """
        return self.get_localized_str()
        

    def round(self, max_decimals=0):
        """
        Round self of to nearest specified decimals and return that result in a new instance.
        """
        ret = type(self)(self)
        if max_decimals > 0:
            ret._value = ret._value.quantize(decimal.Decimal("1." + "0" * max_decimals))
        else:
            ret._value = ret._value.quantize(decimal.Decimal("1"))
        return ret
            
    

    def handle_match(self, match):
        """
        Gives the unit instance a chance to inspect the match and decide how to interpret it.
        Returns a decimal or None if the match is invalid.
        """
        self._match = match
        self.multiplier = self._match.multiplier
        return self.to_decimal(match.number)
    

    def convert_value_to_instance(self, instance):
        """
        Convert the value of self to whatever value the specified instance would be.
        Returns None if the conversion is unsupported.
        """
        if isinstance(instance, (int, float, decimal.Decimal)):
            return type(instance)(self.value)
        elif isinstance(instance, str):
            v = self.to_decimal(instance)
            if not v is None:
                return v
            return self.convert_value_from_instance(self.context.unit(instance))
        elif type(instance) == type(self):
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is Unit:
            return self.value
        return None
        

    def convert_value_from_instance(self, instance):
        """
        Convert the value of the instance to the value of self.
        Returns None if the conversion is unsupported.
        """
        if isinstance(instance, (int, float, decimal.Decimal)):
            return to_decimal(instance)
        elif isinstance(instance, str):
            v = self.to_decimal(instance)
            if not v is None:
                return v
            return self.convert_value_from_instance(self.context.unit(instance))
        elif type(instance) is type(self):
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is Unit:
            return instance.value
        elif isinstance(instance, Unit):
            return instance.convert_value_to_instance(self)
        return None


class Context:
    """
    Keeps track of registered unit types.
    """


    class Item:
        """
        Holds a unit type known by the context.
        """


        class Match:
            """
            Returned by the item when matching unit strings.
            """

            def __init__(self, matched, start, end, prefix, number, multiplier, suffix):
                self.matched = str(matched) if matched else ""
                self.start = int(start)
                self.end = int(end)
                self.prefix = str(prefix) if prefix else ""
                self.number = str(number) if number else ""
                self.multiplier = str(multiplier) if multiplier else ""
                self.suffix = str(suffix) if suffix else ""


            def __repr__(self):
                return "<%s '%s' %d-%d pre='%s' num='%s' mul='%s' suf='%s'>"%(type(self).__name__, self.matched, self.start, self.end, self.prefix, self.number, self.multiplier, self.suffix)
                

        def __init__(self, unit_type, context=None):
            """
            Must provide a context and the unit type.
            """
            t = to_unit_type(unit_type)
            if t is None:
                raise TypeError("argument unit_type is expected to be Unit instance or Unit type, not %s"%type_name(unit_type))
            if not context is None and not isinstance(context, Context):
                raise TypeError("argument context is expected to be Context instance or None, not %s"%type_name(context))
            self._context = context
            self._unit_type = t
            self._regexp = None
            self._regexp_prefix = None
            self._regexp_suffix = None


        def __repr__(self):
            return "<%s '%s'%s>"%(type(self).__name__, self._unit_type.__name__, " (ctx@%d)"%id(self._context) if self._context else "")

        @property
        def context(self):
            return self._context


        @property
        def unit_type(self):
            return self._unit_type
        
        
        def prepare(self):
            """
            Will cause the item to reset it's match algorithm according to it's context and unit_type.
            Should be called when the context or unit_type have been updated.
            """
            thousands_sep = self.context._locale_dict.get("thousands_sep", "")
            decimal_point = self.context._locale_dict.get("decimal_point", ".")
            case_sensetive_symbols = self._unit_type.CASE_SENSETIVE_SYMBOLS
            prefixes = self._unit_type.PREFIXES
            suffixes = self._unit_type.SUFFIXES
            names = list(self._unit_type.NAMES)
            for n in self._unit_type.NAMES:
                tr = self.context.find_translations(n)
                for t in tr:
                    if not t in names:
                        names.append(t)
            multiplier_symbols = []
            multiplier_names = []
            for m in self._unit_type.MULTIPLIERS:
                multiplier_symbols += m.symbols
                multiplier_names += m.names
            for m in self._unit_type.MULTIPLIERS:
                for n in m.names:
                    tr = self.context.find_translations(n)
                    for t in tr:
                        if not t in multiplier_names:
                            multiplier_names.append(t)
            prefixes = [x.strip() for x in prefixes if x.strip()]
            suffixes = [x.strip() for x in suffixes if x.strip()]
            names = [x.strip() for x in names if x.strip()]
            multiplier_symbols = [x.strip() for x in multiplier_symbols if x.strip()]
            multiplier_names = [x.strip() for x in multiplier_names if x.strip()]
            prefix_pattern = ""
            suffix_pattern = ""
            name_pattern = ""
            suffixname_pattern = ""
            msymbol_pattern = ""
            mname_pattern = ""
            msymbolname_pattern = ""
            if prefixes:
                prefix_pattern = "(?P<prefix>(%s%s))"%("" if case_sensetive_symbols else "?i:", "|".join((re.escape(x) for x in prefixes))) if prefixes else ""
            if suffixes:
                suffix_pattern = "(%s%s)"%("" if case_sensetive_symbols else "?i:", "|".join((re.escape(x) for x in suffixes)) if suffixes else "")
            if names:
                name_pattern = "(?i:%s)"%"|".join((re.escape(x) for x in names)) if names else ""
            if suffix_pattern and name_pattern:
                suffixname_pattern = "(?P<suffix>%s|%s)"%(suffix_pattern, name_pattern)
            elif suffix_pattern:
                suffixname_pattern = "(?P<suffix>%s)"%suffix_pattern
            elif name_pattern:
                suffixname_pattern = "(?P<suffix>%s)"%name_pattern
            if suffixname_pattern and prefix_pattern:
                prefix_pattern += "?"
                #suffixname_pattern += "?"
                suffixname_pattern = "(?(prefix)|%s)"%suffixname_pattern
            if multiplier_symbols:
                msymbol_pattern = "(%s%s)"%("" if case_sensetive_symbols else "?i:", "|".join((re.escape(x) for x in multiplier_symbols)) if multiplier_symbols else "")
            if multiplier_names:
                mname_pattern = "(?i:%s)"%"|".join((re.escape(x) for x in multiplier_names)) if multiplier_names else ""
            if msymbol_pattern and mname_pattern:
                msymbolname_pattern = "(?P<multiplier>%s|%s)"%(msymbol_pattern, mname_pattern)
            elif msymbol_pattern:
                msymbolname_pattern = "(?P<multiplier%s)"%msymbol_pattern
            elif mname_pattern:
                msymbolname_pattern = "(?P<multiplier>%s)"%mname_pattern
            if suffixname_pattern:
                msymbolname_pattern += "?"
            locale_pattern = ""
            if thousands_sep:
                locale_pattern += "(\d{1,3}(%s\d{3})*(%s\d*)?)"%(re.escape(thousands_sep), re.escape(decimal_point))
            decimal_pattern = "((\d+\.\d*)|(\.\d+)|\d+)"
            if locale_pattern:
                decimal_locale_pattern = "(%s|%s)?"%(decimal_pattern, locale_pattern)
            else:
                decimal_locale_pattern = "%s?"%decimal_pattern
            number_pattern = "(?P<number>%s([Ee][\+\-]?\d+)?)?"%decimal_locale_pattern
            pattern = "(?=[\w\+\-])%s%s%s%s%s($|(?=\s))"%(
                "(?P<sign>[\+\-])?",
                prefix_pattern,
                number_pattern,
                msymbolname_pattern,
                suffixname_pattern,
            )
            self._regexp = re.compile(pattern)


        def match(self, s):
            """
            Will match the unit string with the context and unit type symbols names and what not.
            Returns a ContextItem.Match instance on match, None otherwise.
            Prefixes have presedence over suffixes and names.
            
            - If the unit has prefixes, suffixes or names defined either must be in the string for a match.
            - Multipliers are optional if there is a prefix, suffix or name available, otherwise the a multiplier must match.
            - Lone numbers are only matched if the unit has no prefixes, suffixes, names or multipliers defined.
            """
            if not self._regexp:
                self.prepare()
            m = self._regexp.search(s)
            if m:
                d = m.groupdict()
                sign = d.get("sign", "")
                prefix = d.get("prefix", "")
                number = d.get("number", "")
                multiplier = d.get("multiplier", "")
                suffix = d.get("suffix", "")
                if sign and number:
                    number = sign + number
                return self.Match(m.group(), m.start(), m.end(), prefix, number, multiplier, suffix)
            return None


    def __init__(self):
        """
        Initialize a new context.
        """
        self._items = []
        self._base_item = self.Item(Unit, self)
        self.number_format_flags = NUMBER_FORMAT_FLAG_SCIENTIFIC | NUMBER_FORMAT_FLAG_DECIMAL | NUMBER_FORMAT_FLAG_LOCALIZED
        self._max_decimals = None
        self._locale_dict = {}
        self._translations = {}
        self._language = None
        self.translation_func = None
        

    def __repr__(self):
        return "<%s %d len=%d%s>"%(type(self).__name__, id(self), len(self), "max_decimals=%d"%self.max_decimals if not self.max_decimals is None else "")
    

    def assert_types(self, *values):
        """
        Takes values that can be Unit instance or Unit types, assert that they are valid and return a list of valid Unit types.
        Will raise AssertionError if any of the values are invalid.
        Additional valid value types are lists, tuples, Context instances and modules/packages IF they have a UNITS symbol that can return valid unit types.
        """
        ret = []
        for value in values:
            if isinstance(value, Unit):
                t = type(value)
                if not t in ret:
                    ret.append(t)
            elif isinstance(value, type) and issubclass(value, Unit):
                if not value in ret:
                    ret.append(value)
            elif isinstance(value, (tuple, list)):
                ret += self.assert_types(*value)
            elif isinstance(value, types.ModuleType):
                if not hasattr(value, "UNITS"):
                    raise AssertionError("module %s does not provide a UNITS list, tuple or callable"%value.__name__)                
                if isinstance(value.UNITS, (list, tuple)):
                    ret += value.UNITS
                elif callable(value.UNITS):
                    l = value.UNITS()
                    if not isinstance(l, (list, tuple)):
                        raise AssertionError("module %s UNITS callable does not return a list or tuple"%value.__name__)
                    for ll in l:
                        if not ll in ret:
                            ret += ll
                else:
                    raise AssertionError("module %s UNITS must be list, tuple or callable"%value.__name__)
            elif isinstance(value, Context):
                ret += self.assert_types(*value)
            else:
                raise AssertionError("invalid type: %s"%type_name(value))
        return ret


    def find(self, value, start=0):
        """
        Find a unit type in the context based on a string, Unit instance or type, return the index of it or -1 if not found.
        Optional argument start will set the starting index of the search.
        """
        if isinstance(value, str):
            s = value.replace(" ", "")
            for i, item in enumerate(self._items):
                m = item.match(s)
                if m and m.matched == s:
                    return i
            return -1
        t = to_unit_type(value)
        if t is None:
            raise TypeError("can only find str, Unit instance or Unit type, not %s"%type_name(value))
        if start < 0:
            start += len(self._items)
        if start < 0:
            start = 0
        for i, item in enumerate(self._items[start:]):
            if t == item._unit_type:
                return i + start
        return -1
                    

    def append(self, *values):
        ret = []
        asserted_types = self.assert_types(*values)
        for asserted_type in asserted_types:
            if not asserted_type in self:
                self._items.append(self.Item(asserted_type, self))
                ret.append(asserted_type)
        return ret
    
    
    def __len__(self):
        return len(self._items)


    def __in__(self, value):
        """
        Checks if a Context.Item, Unit instance, Unit type exists or string matches any of the items.
        """
        return self.find(value) >= 0
    

    def __getitem__(self, index_or_key):
        """
        Get a unit type from the context.
        Index can be integer, Unit instance, unit type or string.
        """
        if isinstance(index_or_key, int):
            return self._items[index_or_key].unit_type
        index = self.find(index_or_key)
        if index < 0:
            raise IndexError("could not match unit: %s"%index_or_key)
        return self._items[index].unit_type


    def __setitem__(self, index_or_key, value):
        """
        Set an item in the context using integer index, string key for matching or a Unit instance/type.
        Index can be integer, Context.Item instance, Unit instance, unit type or string.
        """
        t = self.assert_types(value)[0]
        if t in self:
            raise ValueError("type %s already exists in item %d"%(t.__name__, found))
        if isinstance(index_or_key, int):
            if index_or_key < 0:
                index_or_key += len(self)
            if index_or_key < 0 or index_or_key >= len(self):
                raise IndexError("integer index out of range: %d"%index_or_key)
            self._items[index_or_key] = Context.Item(t, self)
        else:
            index = self.find(index_or_key)
            if index < 0:
                raise IndexError("could not match unit: %s"%index_or_key)
            self._items[index] = Context.Item(t, self)
    

    def __delitem__(self, index_or_key):
        """
        Remove an item from the context using integer index, string key for matching or a Unit instance/type.
        """
        if isinstance(index_or_key, int):
            del self._items[index_or_key]
        else:
            index = self.find(index_or_key)
            if index < 0:
                raise IndexError("could not match unit: %s"%index_or_key)
            del self._items[index]
            

    def __iter__(self):
        """
        Iterate all types in the context.
        """
        return iter((item.unit_type for item in self._items))
    

    def __iadd__(self, value):
        self.append(value)


    def __add__(self, value):
        ret = Context(*self._items)
        ret.append(value)
        return ret


    def get_item_for_unit(self, unit_or_type):
        """
        Return the Context.Item for a specific unit.
        """
        t = to_unit_type(unit_or_type)
        if t:
            if t == Unit:
                return self._base_item
            for item in self._items:
                if t == item.unit_type:
                    return item
            return None
        raise TypeError("argument expected Unit instance or Unit type, not %s"%unit_or_type)


    def update_items(self):
        """
        Updates all context items in the context to reflect the latest context settings.
        Called when something changes in the locale or translations etc.
        """
        for item in self._items:
            item.prepare()
        

    def get_max_decimals(self):
        """
        Max decimals for any value created by the context.
        None: no setting
        -1: no limit
        >= 0: limit to this many decimals.
        """
        return self._max_decimals


    def set_max_decimals(self, value):
        """
        Set max decimals for any value created by the context.
        None: no setting
        -1: no limit
        >= 0: limit to this many decimals.
        """
        if value is None:
            self._max_decimals = None
        elif isinstance(value, int):
            self._max_decimals = max(-1, value)
        elif isinstance(value, str):
            v = to_integer(value)
            if v is None:
                raise ValueError("invalid max decimals: %s"%value)
            self._max_decimals = max(-1, v)
        else:
            raise TypeError("max decimals requires None, int or str, not %s"%type(value).__name__)


    @property
    def max_decimals(self):
        """
        Max decimals for any value created by the context.
        None: no setting
        -1: no limit
        >= 0: limit to this many decimals.
        """
        return self.get_max_decimals()


    @max_decimals.setter
    def max_decimals(self, value):
        self.set_max_decimals(value)


    def get_locale_dict(self):
        """
        Returns a COPY of the locale dict.
        The locale dict should contain the same stuff you would expect in the dict returned from the locale.localeconv().
        """
        return dict(self._locale_dict)


    def set_locale_dict(self, value):
        """
        Set the locale dict. Will update all items.
        The locale dict should contain the same stuff you would expect in the dict returned from the locale.localeconv().
        """
        self._locale_dict = dict(value)
        self.update_items()


    def get_thousands_sep(self):
        """
        Return the string for thousands separator.
        """
        return self._locale_dict.get("thousands_sep", "")


    def set_thousands_sep(self, value):
        """
        Set the string for thousands separator.
        If value is None the key will be removed from the locale dict.
        """
        d = self.get_locale_dict()
        if value is None:
            if "thousands_sep" in d:
                del d["thousands_sep"]
        else:
            d["thousands_sep"] = value
        self.set_locale_dict(d)


    def get_decimal_point(self):
        """
        Return the string for thousands separator.
        """
        r = self._locale_dict.get("decimal_point", "")
        if isinstance(r, str) or r.strip():
            return r
        return "."


    def set_decimal_point(self, value):
        """
        Set the string for thousands separator.
        If value is None the key will be removed from the locale dict.
        """
        d = self.get_locale_dict()
        if value is None:
            if "decimal_point" in d:
                del d["decimal_point"]
        else:
            d["decimal_point"] = value
        self.set_locale_dict(d)


    def add_translations(self, language_code, words_dict=None):
        """
        Add language translations for the context.
        language_code: The language code formatted as xx_YY (case insensetive).
        words_dict: A dictionary of the the standard unit names (as their specification describes their international name) as keys translated to language values.
        If the language already exists the existing dict will be merged with words_dict (words_dict will overwrite any existing translations).

        If words_dict is None (or omitted) language_code is expected to be a dictionary (or anything with .items()) of language codes as keys and translation dicts as values.

        Translations are only for unit and multiplier names.
        """
        if words_dict is None:
            if not hasattr(language_code, "items"):
                raise TypeError("the first argument expected to have 'iter' method, this does not: %s"%type_name(language_code))
            new_translations = dict(self._translations)
            for k, v in language_code.items():
                if not isinstance(k, str):
                    raise TypeError("the key of a language dict must be str, not %s"%type_name(k))
                lc = k.lower()
                spl = k.split("_")
                if len(spl) != 2 or len(spl[0]) != 2 or len(spl[1]) != 2:
                    raise ValueError("the key of a language dict must be a valid language code (require xx_YY): %s"%k)
                d = new_translations.get(lc, {})
                d.update(v)
                new_translations[lc] = d
            self._translations = new_translations
            self.update_items()
        else:
            if not isinstance(language_code, str):
                raise TypeError("language_code expects a str, not %s"%type_name(language_code))
            lc = language_code.lower()
            spl = lc.split("_")
            if len(spl) != 2 or len(spl[0]) != 2 or len(spl[1]) != 2:
                raise ValueError("invalid language code (require xx_YY): %s"%language_code)
            d = self._translations.get(lc, {})
            d.update(words_dict)
            self._translations[lc] = d
            self.update_items()


    def remove_translations(self, language_code=None, words_list=None):
        """
        Remove language translations for the context.
        if language_code is None then all languages will be affected.
        If words_list is None ALL translations for the specified language will be removed, otherwise specific words will be removed.
        The words_list will use the standard names of the units (in english or whatever the specifications say).

        Translations are only for unit and multiplier names.
        """
        if not isinstance(language_code, str) and not language_code is None:
            raise TypeError("language_code expects a str or None, not %s"%type_name(language_code))
        update = False
        if language_code is None:
            if words_list is None:
                self._translations = {}
                update = True
            else:
                for d in self._translations.values():
                    for w in words_list:
                        if w in d:
                            del d[w]
                            update = True
        else:
            lc = language_code.lower()
            spl = lc.split("_")
            if len(spl) != 2 or len(spl[0]) != 2 or len(spl[1]) != 2:
                raise ValueError("invalid language code (require xx_YY): %s"%language_code)
            if words_list is None:
                d = self._translations.get(lc)
                if not d is None:
                    del self._translations[lc]
                    update = True
            else:
                d = self._translations.get(lc, {})
                for w in words_list:
                    if w in d:
                        del d[w]
                        update = True
                self._translations[lc] = d
        if update:
            self.update_items()


    def get_translations(self, language_code=None):
        """
        If language_code is supplied this function will return a copy of the translation dict for that language.
        If language_code is None (or ommited) this function will return a copy of the translation dict for all languages.

        Translations are only for unit and multiplier names.
        """
        if isinstance(language_code, str):
            lc = language_code.lower()
            spl = lc.split("_")
            if len(spl) != 2 or len(spl[0]) != 2 or len(spl[1]) != 2:
                return None
            d = self._translations.get(lc)
            if d:
                return dict(d)
            return None
        elif language_code is None:
            ret = {}
            for k, v in self._translations.items():
                ret[k] = dict(v)
            return ret
        else:
            raise TypeError("expected first argument to be str or None, not %s"%type_name(language_code))


    def find_translations(self, name, language_code=None):
        """
        Translate a unit or multiplier name (their standard names) into a language specific name.
        Returns a list of matching translations.

        If language_code is None then translations for ALL languages will be returned.

        If the name isn't found in any of the context translations and context.translation_func is a callable
        it will be called with the context and the two arguments supplied such as:
        
        translation_func(context, name, language_code=None) -> list()
        """
        if not isinstance(name, str):
            raise TypeError("expected first argument to be str, not %s"%type_name(name))
        if isinstance(language_code, str):
            d = self.get_translations(language_code)
            if d:
                r = d.get(name)
                if r:
                    return [r]
            if self.translation_func:
                return self.translation_func(self, name, language_code)
            return []
        elif language_code is None:
            ret = []
            for v in self._translations.values():
                r = v.get(name)
                if r and not r in ret:
                    ret.append(r)
            if not ret and self.translation_func:
                return self.translation_func(self, name, language_code)
            return ret
        raise TypeError("expected language_code argument to be str or None, not %s"%type_name(language_code))


    def translate(self, name, language_code=None):
        """
        Translate the name.
        If language_code is None the context settings will be used. If those are also None then the first match will be returned.
        If there is no match name is returned as is.
        """
        if not isinstance(name, str):
            raise TypeError("first argument expected a str, not %s"%type_name(name))
        s = name.strip()
        if not s:
            return name
        if language_code:
            if not isinstance(language_code, str):
                raise TypeError("second argument expected a str or None, not %s"%type_name(language_code))
            lc = language_code.lower()
            spl = lc.split("_")
            if len(spl) != 2 or len(spl[0]) != 2 or len(spl[1]) != 2:
                raise ValueError("invalid language code (require xx_YY): %s"%language_code)
            language_code = lc
        else:
            language_code = self._language
        if language_code:
            d = self._translations.get(language_code)
            if d:
                v = d.get(s, s)
                if v:
                    return v
        else:
            for d in self._translations.values():
                v = d.get(s)
                if v:
                    return v
        return name
    

    def get_language(self):
        """
        Get the preferred language when formatting localized strings from units.
        Can be None to use the original names only.
        """
        return self._language


    def set_language(self, language_code):
        """
        Set the preferred language when formatting localized strings from units.
        Can be None to use the original names only.
        """
        if isinstance(language_code, str):
            lc = language_code.lower()
            spl = lc.split("_")
            if len(spl) != 2 or len(spl[0]) != 2 or len(spl[1]) != 2:
                raise ValueError("invalid language code (require xx_YY): %s"%language_code)
            self._language = "%s_%s"%(spl[0], spl[1].upper())
        elif language_code is None:
            self._language_code = None
        else:
            raise TypeError("expected first argument to be str or None, not %s"%type_name(language_code))
        

    def search(self, s, multi=False, exact=True):
        """
        Try to match any unit with the string, Unit instance or Unit type.
        Returns a a list of two-element tuples of the match instance and the matched type.
        If optional argument multi is set to True the returned list may contain more than one tuple.
        If no match is found an empty list will be returned.

        This function can be used to spot where multiple units may match the same expression or if the same type is in multiple context items.

        This function will add the base Unit to the search if all others fail.
        """
        ret = []
        items = list(self._items)
        items.append(self._base_item)
        if isinstance(s, str):
            for item in items:
                m = item.match(s)
                if m and (not exact or m.matched == s):
                    ret.append((m, item._unit_type))
                    if not multi:
                        return ret
        elif isinstance(s, Unit):
            t = type(s)
            for item in items:
                if t == item._unit_type:
                    ret.append((m, item._unit_type))
                    if not multi:
                        return ret
        elif isinstance(s, type) and issubclass(s, Unit):
            for item in items:
                if s == item._unit_type:
                    ret.append((m, item._unit_type))
                    if not multi:
                        return ret
        else:
            raise TypeError("expected first argument to be str, Unit instance or Unit type, not %s"%type_name(s))
        return ret


    def determine_type(self, value):
        """
        Return a type that matches an integer, float, Decimal or string.
        Integers, floats, decimals, None and strings containing only numbers (or empty) will return the base Unit type.
        """
        if value is None:
            return Unit
        if isinstance(value, (int, float, decimal.Decimal)):
            return Unit
        if not isinstance(value, str):
            raise TypeError("expected int, float, Decimal or str, not %s"%type(value).__name__)
        value = value.strip()
        if not value:
            return Unit
        if is_decimal(value, self.number_format_flags, self.get_locale_dict().get("thousands_sep", ""), self.get_locale_dict().get("decimal_point", ".")):
            return Unit
        index = self.find(value)
        if index >= 0:
            return self._items[index].unit_type
        return None


    def unit(self, value):
        """
        Instance and return a new unit. Raises ValueError if value is invalid.
        """
        t = self.determine_type(value)
        if not t:
            raise ValueError("invalid unit type: %s"%value)
        ret = t(value, context=self)
        return ret


    def unit_noexc(self, value):
        """
        Instance and return a new unit. Returns None if value is invalid.
        """
        t = self.determine_type(value)
        if not t:
            return None
        ret = t(value, context=self)
        return ret


    def parse(self, s, offset=0, line=0, pos=0):
        """
        Used by Context.calc() to parse text into nested lists of units and operations.
        """
        remainder = str(s)
        while remainder.find("  ") >= 0:
            remainder = remainder.replace("  ", " ")
        elements = []
        while remainder:
            while remainder and remainder[0].isspace():
                if remainder[0] == '\n':
                    pos = 0
                    line += 1
                else:
                    pos += 1
                offset += 1
                remainder = remainder[1:]
            if not remainder:
                break
            if remainder[0] == "(":
                count = 0
                group = ""
                for i in range(len(remainder)):
                    if remainder[i] == "(":
                        count += 1
                    elif remainder[i] == ")":
                        count -= 1
                    if count == 0:                        
                        group = remainder[1:i]
                        remainder = remainder[i+1:]
                        offset += i + 1
                        pos += i + 1
                        break
                if count != 0:
                    raise ValueError("unterminated group at line %d pos %d (offset %d): %s"%(line, pos, offset, remainder[0]))
                if group:
                    elements.append(self.parse(group, offset + 1, line, pos + 1))
                continue
            if remainder[:2] == "+ " or remainder[:2] == "- ":
                if elements and isinstance(elements[-1], str) and elements[-1] in "+-":
                    raise ValueError("invalid operation at line %d pos %d (offset %d): %s"%(line, pos, offset, remainder[0]))
                if remainder[0] == "-":
                    elements.append(remainder[0])
                remainder = remainder[2:]
                offset += 2
                pos += 2
                continue
            if remainder[0] in "*/":
                if not elements or not isinstance(elements[-1], (list, tuple, Unit)):
                    raise ValueError("invalid operation at line %d pos %d (offset %d): %s"%(line, pos, offset, remainder[0]))
                elements.append(remainder[0])
                remainder = remainder[1:]
                offset += 1
                pos += 1
                continue
            rl = self.search(remainder, False, False)
            if rl:
                match = rl[0][0]
                ut = rl[0][1]
                if match and ut:
                    if match.start != 0:
                        raise ValueError("invalid unit at line %d pos %d (offset %d): %s"%(line, pos, offset, remainder[:match.start]))
                    elements.append(ut(match.matched))
                    remainder = remainder[match.end:]
                    offset += len(match.matched)
                    pos += len(match.matched)
                    continue
            raise ValueError("invalid symbol at line %d pos %d (offset %d): %s"%(line, pos, offset, remainder[0]))
        return elements
        

    def _calc(self, units):
        if isinstance(units, Unit):
            units = [units]
        elif not isinstance(units, (list, tuple)):
            raise TypeError("expected a list or tuple of Unit instances and string operators, not %s"%type(units).__name__)
        units = list(units)
        for i, u in enumerate(units):
            if isinstance(u, (list, tuple)):
                units[i] = self._calc(u)
        i = 0
        while i < len(units):
            other = units[i]
            if isinstance(other, str) and other in "/*":
                if i == 0 or i >= len(units):
                    raise ValueError("operator %s requires two unit operands"%other)
                lvalue = self._calc(other) if isinstance(units[i - 1], (list, tuple)) else units[i - 1]
                if not isinstance(lvalue, Unit):
                    raise ValueError("operator %s lvalue must be Unit, not %s"%(other, type(units[i-1]).__name__))
                rvalue = self._calc(other) if isinstance(units[i + 1], (list, tuple)) else units[i + 1]
                if not isinstance(rvalue, Unit):
                    raise ValueError("operator %s rvalue must be Unit, not %s"%(other, type(units[i+1]).__name__))
                if other == "*":
                    result = lvalue * rvalue
                elif other == "/":
                    result = lvalue / rvalue
                del units[i:i+2]
                units[i-1] = result
            else:
                i += 1
        operator = ""
        if units:
            ret = units[0]
            for u in units[1:]:
                if isinstance(u, str) and u in "+-":
                    if operator:
                        raise ValueError("two operators in a row, not good probably a bug")
                    operator = u
                elif isinstance(u, Unit):
                    if operator == "-":
                        ret = ret - u
                    else:
                        ret = ret + u
                    operator = ""
            return ret
        return Unit(0)


    def calc(self, s):
        """
        Parse some math and return nested lists of units and operations.
        """
        return self._calc(self.parse(s))

    
        
    
    
_DEFAULT_CONTEXT = Context()
_CONTEXT = _DEFAULT_CONTEXT


def get_context():
    return _CONTEXT


def set_context(context):
    """
    Set the current global context.
    """
    if context is None:
        _CONTEXT = _DEFAULT_CONTEXT
    elif isinstance(context, Context):
        _CONTEXT = CONTEXT
    else:
        raise TypeError("invalid context: %s"%context)


def unit(value):
    """
    Instance and return a new unit from the default context. Raises ValueError if value is invalid.
    """
    return get_context().unit(value)


def unit_noexc(value):
    """
    Instance and return a new unit from the default context. Returns None if value is invalid.
    """
    return get_context().unit_exc(value)
