# -*- coding: utf-8 -*-
#
# SI units (and friends).
#
# Part Python Units, a small library for parsing and converting various units.
#
# MIT License

# Copyright (c) 2018 Peter Gebauer

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import decimal
from units import *


class UnitSI(Unit):
    """
    A base for all SI units, include multipliers for the SI prefixes.
    """

    SYSTEM = "si"

    MULTIPLIERS = (
        UnitMultiplier("Y", "yotta", decimal.Decimal("1E24")),
        UnitMultiplier("Z", "zetta", decimal.Decimal("1E21")),
        UnitMultiplier("E", "exa", decimal.Decimal("1E18")),
        UnitMultiplier("P", "peta", decimal.Decimal("1E15")),
        UnitMultiplier("T", "tera", decimal.Decimal("1E12")),
        UnitMultiplier("G", "giga", decimal.Decimal("1E9")),
        UnitMultiplier("M", "mega", decimal.Decimal("1E6")),
        UnitMultiplier("k", "kilo", decimal.Decimal("1E3")),
        UnitMultiplier("h", "hecto", decimal.Decimal("1E2")),
        UnitMultiplier("da", "deca", decimal.Decimal("1E1")),
        UnitMultiplier("d", "deci", decimal.Decimal("1E-1")),
        UnitMultiplier("c", "centi", decimal.Decimal("1E-2")),
        UnitMultiplier("m", "milli", decimal.Decimal("1E-3")),
        UnitMultiplier("μ|u", "micro", decimal.Decimal("1E-6")),
        UnitMultiplier("n", "nano", decimal.Decimal("1E-9")),
        UnitMultiplier("p", "pico", decimal.Decimal("1E-12")),
        UnitMultiplier("f", "femto", decimal.Decimal("1E-15")),
        UnitMultiplier("a", "atto", decimal.Decimal("1E-18")),
        UnitMultiplier("z", "zepto", decimal.Decimal("1E-21")),
        UnitMultiplier("y", "yocto", decimal.Decimal("1E-24")),
    )


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitSI:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitSI:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        return super().convert_value_from_instance(instance)
    
        

class UnitSIGram(UnitSI):
    """
    SI gram.
    """

    MEASURE = "mass"
    SUFFIXES = ("g",)
    NAMES = ("gram", "grams", "gramme", "grammes")
    
    
    def convert_value_to_instance(self, instance):
        if type(instance) is UnitSIGram:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitSIGram:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        return super().convert_value_from_instance(instance)
    

class UnitSITonne(UnitSI):
    """
    Metric tonne (friend of SI).
    """

    MEASURE = "mass"
    SUFFIXES = ("t",)
    NAMES = ("tonne", "tonnes", "ton", "tons")
    
    
    def convert_value_to_instance(self, instance):
        if type(instance) is UnitSITonne:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitSIGram:
            return (self.value * self.multiplier_value * 1000000) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitSITonne:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitSIGram:
            return (instance.value * instance.multiplier_value / 1000000) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitSIMetre(UnitSI):
    """
    SI metre.
    """

    MEASURE = "length"
    SUFFIXES = ("m",)
    NAMES = ("metre", "metres", "meter", "meters")


    def __mul__(self, other):
        if isinstance(other, UnitSIMetre):
            if UnitSISquareMetre in self.context:
                return UnitSISquareMetre(self.value * self.multiplier_value * other.value * other.multiplier_value)
        return super().__mul__(other)
    

    def convert_value_to_instance(self, instance):
        if type(instance) is UnitSIMetre:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitSIMetre:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        return super().convert_value_from_instance(instance)
    

class UnitSILitre(UnitSI):
    """
    SI litre.
    """

    MEASURE = "volume"
    SUFFIXES = ("l", "L")
    NAMES = ("litre", "litres", "liter", "liters")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitSILitre:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitSILitre:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitSISquareMetre(UnitSI):
    """
    SI square metre.
    """

    MEASURE = "area"
    SUFFIXES = ("m²", "m2")
    NAMES = ("square metre", "square metres", "square meter", "square meters", "squaremetre", "squaremetres", "squaremeter", "squaremeters")


    def __mul__(self, other):
        if isinstance(other, UnitSIMetre):
            if UnitSICubicMetre in self.context:
                return UnitSICubicMetre(self.value * self.multiplier_value * other.value * other.multiplier_value)
        return super().__mul__(other)


    def __truediv__(self, other):
        if isinstance(other, UnitSIMetre):
            if UnitSIMetre in self.context:
                return UnitSIMetre((self.value * self.multiplier_value) / (other.value * other.multiplier_value))
        return super().__truediv__(other)
    
    
    def convert_value_to_instance(self, instance):
        if type(instance) is UnitSISquareMetre:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitSISquareMetre:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitSICubicMetre(UnitSI):
    """
    SI cubic metre.
    """

    MEASURE = "volume"
    SUFFIXES = ("m³", "m3")
    NAMES = ("cubic metre", "cubic metres", "cubic meter", "cubic meters", "cubicmetre", "cubicmetres", "cubicmeter", "cubicmeters")


    def __truediv__(self, other):
        if isinstance(other, UnitSIMetre):
            if UnitSISquareMetre in self.context:
                return UnitSISquareMetre((self.value * self.multiplier_value) / (other.value * other.multiplier_value))
        return super().__truediv__(other)
    
    
    def convert_value_to_instance(self, instance):
        if type(instance) is UnitSICubicMetre:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitSILitre:
            return (self.value * (self.multiplier_value ** 3) * 1000) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitSICubicMetre:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitSILitre:
            return (((instance.value * instance.multiplier_value) / 1000) / self.multiplier_value ** 3) / self.multiplier_value
        return super().convert_value_from_instance(instance)



class UnitSIKelvin(UnitSI):
    """
    SI kelvin.
    """

    MEASURE = "temperature"
    SUFFIXES = ("K",)
    NAMES = ("kelvin", "kelvins")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitSIKelvin:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitSIKelvin:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitSICelsius(UnitSI):
    """
    Celsius (friend of SI)
    """

    MEASURE = "temperature"
    SUFFIXES = ("°C", "C")
    NAMES = ("celsius", "celsius")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitSICelsius:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitSIKelvin:
            return (self.value * self.multiplier_value + decimal.Decimal("273.15")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitSICelsius:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitSIKelvin:
            return (instance.value * instance.multiplier_value - decimal.Decimal("273.15")) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitSIHertz(UnitSI):
    """
    SI hertz.
    """

    MEASURE = "frequency"
    SUFFIXES = ("Hz", "hz")
    NAMES = ("hertz", "hertz")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitSIKelvin:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitSIKelvin:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitSIJoule(UnitSI):
    """
    SI hertz.
    """

    MEASURE = "energy"
    SUFFIXES = ("J",)
    NAMES = ("joule", "joules")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitSIJoule:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitSIJoule:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        return super().convert_value_from_instance(instance)


class UnitSICalorie(UnitSI):
    """
    Calorie (friend of food industry, relates directly to joule).
    """

    MEASURE = "energy"
    SUFFIXES = ("cal",)
    NAMES = ("calorie", "calories")


    def convert_value_to_instance(self, instance):
        if type(instance) is UnitSICalorie:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif type(instance) is UnitSIJoule:
            return (self.value * self.multiplier_value * decimal.Decimal("4.184")) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitSICalorie:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif type(instance) is UnitSIJoule:
            return (instance.value * instance.multiplier_value / decimal.Decimal("4.184")) / self.multiplier_value
        return super().convert_value_from_instance(instance)


UNITS = [UnitSIGram, UnitSITonne, UnitSILitre, UnitSIMetre, UnitSISquareMetre, UnitSICubicMetre, UnitSIKelvin, UnitSICelsius, UnitSIHertz, UnitSIJoule, UnitSICalorie]
