# -*- coding: utf-8 -*-
#
# Currency units.
#
# Part Python Units, a small library for parsing and converting various units.
#
# MIT License

# Copyright (c) 2018 Peter Gebauer

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import decimal
from units import *


class UnitCurrency(Unit):
    """
    Base for currency.
    """

    MEASURE = "currency"
    CASE_SYMBOLS = False
    CENTI_TYPE = None
    HECTO_TYPE = None
    MAX_DECIMALS = 2

    
    def convert_value_to_instance(self, instance):
        if type(instance) is UnitCurrency:
            return (self.value * self.multiplier_value) / instance.multiplier_value
        elif self.CENTI_TYPE and type(instance) is self.CENTI_TYPE:
            return (self.value * self.multiplier_value * 100) / instance.multiplier_value
        elif self.HECTO_TYPE and type(instance) is self.HECTO_TYPE:
            return (self.value * self.multiplier_value / 100) / instance.multiplier_value
        return super().convert_value_to_instance(instance)
        

    def convert_value_from_instance(self, instance):
        if type(instance) is UnitCurrency:
            return (instance.value * instance.multiplier_value) / self.multiplier_value
        elif self.CENTI_TYPE and type(instance) is self.CENTI_TYPE:
            return (instance.value * instance.multiplier_value / 100) / self.multiplier_value
        elif self.HECTO_TYPE and type(instance) is self.HECTO_TYPE:
            return (instance.value * instance.multiplier_value * 100) / self.multiplier_value
        return super().convert_value_from_instance(instance)


def create(name, prefixes, suffixes, names, multipliers=None, centi=None, hecto=None):
    """
    Create your own currency type easily.
    name: The name of the type.
    prefixes: A list/tuple of prefixes for PREFIXES override.
    suffixes: A list/tuple of suffixes for SUFFIXES override.
    names: A list/tuple of names for NAMES override.
    multipliers: A list/tuple of multipliers for MULTIPLIERS override.
    centi: A unit type that converts 1 of our unit into 100 of the specified type.
    hecto: A unit type that converts 100 of our unit to 1 of the specified type.
    """
    if isinstance(prefixes, str):
        prefixes = [prefixes]
    if isinstance(suffixes, str):
        suffixes = [suffixes]
    if isinstance(names, str):
        names = [names]
    d = {"PREFIXES": prefixes, "SUFFIXES": suffixes, "NAMES": names}
    if multipliers:
        d["MULTIPLIERS"] = multipliers
    if centi:
        d["CENTI_TYPE"] = centi
    if hecto:
        d["HECTO_TYPE"] = hecto
    return type(name, (UnitCurrency,), d)


def create_global(name, prefixes, suffixes, names, multipliers=None, centi=None, hecto=None):
    """
    Helper that sets the global after creating a new currency type.
    """
    globals()[name] = create(name, prefixes, suffixes, names, multipliers, centi, hecto)


create_global("UnitCurrencyUSDollar", "$", "USD", ("Dollar", "Dollars", "US dollar", "US dollars"), centi="UnitCurrencyUSCent")
create_global("UnitCurrencyUSCent", "", ("¢", "c"), ("Cent", "Cents", "US cent", "US cents"), hecto="UnitCurrencyUSDollar")
create_global("UnitCurrencyPoundSterling", "£", "GBP", ("Pound", "Pounds", "Pound sterling", "Pounds sterling", "British pound", "British pounds"), centi="UnitCurrencyBritishPenny")
create_global("UnitCurrencyBritishPenny", "", "p", ("Penny", "Pence", "British penny", "British pence"), hecto="UnitCurrencyPoundSterling")
create_global("UnitCurrencyEuro", "€", "EUR", ("Euro", "Euros"), centi="UnitCurrencyEuroCent")
create_global("UnitCurrencyEuroCent", "", "c", ("Cent", "Cents", "Euro cent", "Euro cents"), hecto="UnitCurrencyEuro")
create_global("UnitCurrencySwedishKrona", "", ("SEK", "kr"), ("Krona", "Kronor", "Swedish krona", "Swedish kronor"))
create_global("UnitCurrencyNorwegianKrone", "", ("NOK", "kr"), ("krone", "kroner", "Norwegian krone", "Norwegian kroner"), centi="UnitCurrencyNorwegianOre")
create_global("UnitCurrencyNorwegianOre", "", "ø", ("Øre", "Øre", "Norwegian øre", "Norwegian øre", "Ore", "Ore", "Norwegian ore", "norwegian ore"), hecto="UnitCurrencyNorwegianKrone")
create_global("UnitCurrencyDanishKrone", "", ("DKK", "kr"), ("krone", "kroner", "Danish krone", "Danish kroner"), centi="UnitCurrencyDanishOre")
create_global("UnitCurrencyDanishOre", "", "ø", ("Øre", "Øre", "Danish øre", "Danish øre", "Ore", "Ore", "Danish ore", "danish ore"), hecto="UnitCurrencyDanishKrone")
create_global("UnitCurrencyCzechKoruna", "", ("CZK", "Kč"), ("Koruna", "Koruna", "Czech koruna", "Czech koruna"))



UNITS = []
for k, v in dict(globals()).items():
    if isinstance(v, type) and issubclass(v, UnitCurrency):
        if v.CENTI_TYPE and isinstance(v.CENTI_TYPE, str):
            v.CENTI_TYPE = globals()[v.CENTI_TYPE]
        if v.HECTO_TYPE and isinstance(v.HECTO_TYPE, str):
            v.HECTO_TYPE = globals()[v.HECTO_TYPE]
        UNITS.append(v)
