# -*- coding: utf-8 -*-
#
# Unit test for temperatures.
#
# Part Python Units, a small library for parsing and converting various units.
#
# Credit to Sølve Monteiro for this unit test.
#
# MIT License

# Copyright (c) 2018 Peter Gebauer

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import unittest
from units import Context
from units import si, imperial

    
    
class KelvinCelciusConversionTest(unittest.TestCase):
    """
    Test temperature units for Kelvin, Celsius and Fahrenheit.
    """
    

    def setUp(self):
        self.ctx = Context()
        self.ctx.max_decimals = 4
        self.ctx.append(si, imperial.UnitImperialFahrenheit)

        
    def testConvertFromKelvinToKelvin(self):
        self.assertEqual(self.ctx.unit("0K").convert("K"), "0K")
        self.assertEqual(self.ctx.unit("123K").convert("K"), "123K")
        self.assertEqual(self.ctx.unit("123K").convert("K"), "123K")
        self.assertEqual(self.ctx.unit("273.15K").convert("K"), "273.15K")

        
    def testConvertKelvinToCelcius(self):
        self.assertEqual(self.ctx.unit("0K").convert("C"), "-273.15C")
        self.assertEqual(self.ctx.unit("273.15K").convert("C"), "0C")
        self.assertEqual(self.ctx.unit("373.15K").convert("C"), "100C")


    def testConvertCelciusToKelvin(self):
        self.assertEqual(self.ctx.unit("-273.15C").convert("K"), "0K")
        self.assertEqual(self.ctx.unit("0C").convert("K"), "273.15K")
        self.assertEqual(self.ctx.unit("100C").convert("K"), "373.15K")


    def testFarenheitToKelvin(self):
        self.assertEqual(self.ctx.unit("-459.67F").convert("K"), "0K")
        self.assertEqual(self.ctx.unit("0F").convert("K"), "255.3722K")
        self.assertEqual(self.ctx.unit("32F").convert("K"), "273.15K")
        self.assertEqual(self.ctx.unit("212F").convert("K"), "373.15K")
        

if __name__ == "__main__":
    unittest.main()
