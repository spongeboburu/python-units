# -*- coding: utf-8 -*-
#
# Unit test for simple numbers and symbols matching.
#
# Part Python Units, a small library for parsing and converting various units.
#
# Credit to Sølve Monteiro for this unit test.
#
# MIT License

# Copyright (c) 2018 Peter Gebauer

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import unittest
import decimal
from units import Context, Unit, UnitMultiplier
from units import si, imperial



class TestAlpha(Unit):
    PREFIXES = ("a",)
    SUFFIXES = ("a",)
    NAMES = ("alpha",)


class TestMultiplier(Unit):
    MULTIPLIERS = (UnitMultiplier("k|K", "kilo", 1000), UnitMultiplier("g|G", "giga", 1000000000))
    

class TestBeta(TestMultiplier):
    PREFIXES = ("b",)
    SUFFIXES = ("b",)
    NAMES = ("beta",)


class TestNoSuffix(Unit):
    PREFIXES = ("pre",)
    NAMES = ("prefix",)
    MULTIPLIERS = (UnitMultiplier("k|K", "kilo", 1000), UnitMultiplier("g|G", "giga", 1000000000))


class TestNoPrefix(Unit):
    SUFFIXES = ("suf",)
    NAMES = ("suffix",)
    MULTIPLIERS = (UnitMultiplier("k|K", "kilo", 1000), UnitMultiplier("g|G", "giga", 1000000000))


class TestNoSuffixOrName(Unit):
    PREFIXES = ("DING",)


class TestNoCase(Unit):
    PREFIXES = ("BING",)
    SUFFIXES = ("BONG",)
    CASE_SENSETIVE_SYMBOLS = False
    
    
class UnitMatchTest(unittest.TestCase):
    """
    Test the basics of unit matching.
    """

    def setUp(self):
        self.ctx = Context()
        self.ctx.max_decimals = 4
        self.ctx.append(TestAlpha, TestMultiplier, TestBeta, TestNoSuffix, TestNoPrefix, TestNoSuffixOrName, TestNoCase)


    def testGeneral(self):
        self.assertEqual(self.ctx.determine_type(""), Unit)
        self.assertEqual(self.ctx.determine_type(1), Unit)
        self.assertEqual(self.ctx.determine_type(1.1), Unit)
        self.assertEqual(self.ctx.determine_type(decimal.Decimal("1.5")), Unit)
        

    def testNumbers(self):
        self.assertEqual(self.ctx.determine_type("100"), Unit)
        self.assertEqual(self.ctx.determine_type(" 100 "), Unit)
        self.assertEqual(self.ctx.determine_type(" +100 "), Unit)
        self.assertEqual(self.ctx.determine_type(" -100 "), Unit)
        self.assertEqual(self.ctx.determine_type("+1.45E-3"), Unit)
        self.assertEqual(self.ctx.determine_type("-1000.45E2"), Unit)
        self.assertEqual(self.ctx.determine_type("-10.45E+2"), Unit)
        self.assertEqual(self.ctx.determine_type("+.45E-3"), Unit)
        self.assertEqual(self.ctx.determine_type("-.45E2"), Unit)
        self.assertEqual(self.ctx.determine_type("-.45E+20"), Unit)
        self.assertEqual(self.ctx.determine_type("+1.E-3"), Unit)
        self.assertEqual(self.ctx.determine_type("-2.E2"), Unit)
        self.assertEqual(self.ctx.determine_type("X-1000.45E2"), None)
        self.assertEqual(self.ctx.determine_type("  1  X"), None)
        

    def testBasicMatching(self):
        self.assertEqual(self.ctx.determine_type("-123.321E+3alpha"), TestAlpha)
        self.assertEqual(self.ctx.determine_type("-123.321E+3a"), TestAlpha)
        self.assertEqual(self.ctx.determine_type("-a123.321E+3"), TestAlpha)
        self.assertEqual(self.ctx.determine_type("-a123.321E+3alpha"), None)
        self.assertEqual(self.ctx.determine_type("-123.321E+3alphax"), None)
        self.assertEqual(self.ctx.determine_type("-123.321E+3ta"), None)
        self.assertEqual(self.ctx.determine_type("-a123.321E+3t"), None)
        

    def testMultiplierMatching(self):
        self.assertEqual(self.ctx.determine_type("-123.321E+3k"), TestMultiplier)
        self.assertEqual(self.ctx.determine_type("-123.321E+3K"), TestMultiplier)
        self.assertEqual(self.ctx.determine_type("-123.321E+3g"), TestMultiplier)
        self.assertEqual(self.ctx.determine_type("-123.321E+3G"), TestMultiplier)
        self.assertEqual(self.ctx.determine_type("-123.321E+3kIlO"), TestMultiplier)
        self.assertEqual(self.ctx.determine_type("-123.321E+3GiGa"), TestMultiplier)
        self.assertEqual(self.ctx.determine_type("-b223.321E+3k"), TestBeta)
        self.assertEqual(self.ctx.determine_type("-323.321E+3kbeta"), TestBeta)
        self.assertEqual(self.ctx.determine_type("-423.321E+3kilobeta"), TestBeta)
        self.assertEqual(self.ctx.determine_type("-523.321E+3kilob"), TestBeta)
        self.assertEqual(self.ctx.determine_type("-623.321E+3kb"), TestBeta)


    def testNoSuffix(self):
        self.assertEqual(self.ctx.determine_type("-123.321E+3prefix"), TestNoSuffix)
        self.assertEqual(self.ctx.determine_type("-pre123.321E+3"), TestNoSuffix)
        self.assertEqual(self.ctx.determine_type("-pre123.321E+3prefix"), None)
        self.assertEqual(self.ctx.determine_type("-123.321E+3prefixx"), None)
        self.assertEqual(self.ctx.determine_type("-123.321E+3Gprefix"), TestNoSuffix)
        self.assertEqual(self.ctx.determine_type("-pre123.321E+3G"), TestNoSuffix)
        self.assertEqual(self.ctx.determine_type("-pre123.321E+3gigaprefix"), None)
        self.assertEqual(self.ctx.determine_type("-pre123.321E+3giga"), TestNoSuffix)


    def testNoPrefix(self):
        self.assertEqual(self.ctx.determine_type("-123.321E+3suffix"), TestNoPrefix)
        self.assertEqual(self.ctx.determine_type("-suf123.321E+3"), None)
        self.assertEqual(self.ctx.determine_type("-suf123.321E+3suffix"), None)
        self.assertEqual(self.ctx.determine_type("-123.321E+3suffixx"), None)
        self.assertEqual(self.ctx.determine_type("-123.321E+3Gsuffix"), TestNoPrefix)
        self.assertEqual(self.ctx.determine_type("-suf123.321E+3G"), None)
        self.assertEqual(self.ctx.determine_type("-suf123.321E+3gigasuffix"), None)
        self.assertEqual(self.ctx.determine_type("-suf123.321E+3giga"), None)


    def testNoSuffixOrName(self):
        self.assertEqual(self.ctx.determine_type("-123.321E+3DING"), None)
        self.assertEqual(self.ctx.determine_type("-DING123.321E+3"), TestNoSuffixOrName)
        self.assertEqual(self.ctx.determine_type("-DING123.321E+3DING"), None)
        self.assertEqual(self.ctx.determine_type("-DING123.321E+3DING"), None)
        self.assertEqual(self.ctx.determine_type("-DING123.321E+3"), TestNoSuffixOrName)


    def testCase(self):
        self.assertEqual(self.ctx.determine_type("-123.321E+3ALPHA"), TestAlpha)
        self.assertEqual(self.ctx.determine_type("-123.321E+3A"), None)
        self.assertEqual(self.ctx.determine_type("-A123.321E+3"), None)
        self.assertEqual(self.ctx.determine_type("-123.321E+3bOnG"), TestNoCase)
        self.assertEqual(self.ctx.determine_type("-bInG123.321E+3"), TestNoCase)
        

if __name__ == "__main__":
    unittest.main()
