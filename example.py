#!/bin/env python3
# -*- coding: utf-8 -*-
#
# Part Python Units, a small library for parsing and converting various units.
#
# MIT License

# Copyright (c) 2018 Peter Gebauer

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import decimal
from units import get_context, unit
from units import si, imperial, computer


if __name__ == "__main__":
    # This library is using decimal.Decimal to store values, so we can fiddle with that
    decimal.getcontext().prec = 18
    # Register unit types, a list of types or modules with unit types
    get_context().append(si, computer, imperial.UnitImperialPound, imperial.UnitImperialFahrenheit)
    # Convert easily
    print("Volume conversion: ", unit("2m³").convert("l"))
    print("Temperature conversion: ", unit("70F").convert("C", max_decimals=1))
    # Automatic conversion between compatible units during arithmetic operations
    print("Data addition:", unit("1024kB") + unit("8Mb"))
    print("Mass subtraction:", unit("10kg") - unit("2.5lbs"))
    # Multiplication is allowed in certain cases, like length * length = area
    print("Length multiplication:", unit("2m") * unit("3m"))
    # Coercing between incompatible units using a factor like density
    print("Volume to mass for oil:", unit("10l").coerce("kg", 900))
    # Using the built-in expression evaluator you can write something like this and get a value back
    print("Expression:", get_context().calc("(4kg - (1kg + 4.409245243697551614459476026lbs) * 2) / -2"))
    # Change the number of decimals per context or instance to override the class default
    get_context().max_decimals = 2
    print("Context limit:", unit("1.12345678"))
    # Rounding
    print("Rounding:", unit("1.12345678").round())
    # Simple locale numbers
    get_context().set_locale_dict({"thousands_sep": ","}) # can use locale.localeconv()
    print("Locale number:", unit("100,200,300.400500"))
    # Simple language support using dicts or a callback function
    get_context().add_translations("ru_RU", {"metre": "метр", "metres": "метры", "kilo": "кило"})
    print("Using translated names:", unit("15km").lstr)
