# Python Units

A small library to parse and convert various units. MIT license.

NOTE it is still early in development, the conversions may be incorrect and/or contain bugs.

# TODO

- Check that all the conversions are correct.


# Example

```python
import decimal
from units import get_context, unit
from units import si, imperial, computer


if __name__ == "__main__":
    # This library is using decimal.Decimal to store values, so we can fiddle with that
    decimal.getcontext().prec = 18
    # Register unit types, a list of types or modules with unit types
    get_context().append(si, computer, imperial.UnitImperialPound, imperial.UnitImperialFahrenheit)
    # Convert easily
    print("Volume conversion: ", unit("2m³").convert("l"))
    print("Temperature conversion: ", unit("70F").convert("C", max_decimals=1))
    # Automatic conversion between compatible units during arithmetic operations
    print("Data addition:", unit("1024kB") + unit("8Mb"))
    print("Mass subtraction:", unit("10kg") - unit("2.5lbs"))
    # Multiplication is allowed in certain cases, like length * length = area
    print("Length multiplication:", unit("2m") * unit("3m"))
    # Coercing between incompatible units using a factor like density
    print("Volume to mass for oil:", unit("10l").coerce("kg", 900))
    # Using the built-in expression evaluator you can write something like this and get a value back
    print("Expression:", get_context().calc("(4kg - (1kg + 4.409245243697551614459476026lbs) * 2) / -2"))
    # Change the number of decimals per context or instance to override the class default
    get_context().max_decimals = 2
    print("Context limit:", unit("1.12345678"))
    # Rounding
    print("Rounding:", unit("1.12345678").round())
    # Simple locale numbers
    get_context().set_locale_dict({"thousands_sep": ","}) # can use locale.localeconv()
    print("Locale number:", unit("100,200,300.400500"))
    # Simple language support using dicts or a callback function
    get_context().add_translations("ru_RU", {"metre": "метр", "metres": "метры", "kilo": "кило"})
    print("Using translated names:", unit("15km").lstr)
 ```

Output:
```
Volume conversion:  2000l
Temperature conversion:  21.1°C
Data addition: 2048kB
Mass subtraction: 8.866019075kg
Length multiplication: 6m²
Volume to mass for oil: 9kg
Expression: 1kg
Context limit: 1.12
Rounding: 1
Locale number: 300.4
Using translated names: 15.00 километры
```


# Installation

Still early in development, if you want to contribute with an installer that's fine.
